sconst _ = require('lodash')
const checkTypes = require('check-types')
const pendingActivities = require('./pendingActivities.js')
module.exports = {
    handleState: function(args){
        checkTypes.assert.object(args)
        checkTypes.assert.object(args.state)
        if(!checkTypes.object(state.complete)){
            args.state.complete = _args;
        }
        else{
            _.merge(args.state.complete, _args);
        }
        return args.state;
    },
    methods: {
        addCompleteActivities: function(args){
            this.handleState(args);
            checkTypes.assert.object(args);
            checkTypes.assert.object(args.elements);
            if(checkTypes.object(args.options)){
                args.options = {}
            }
            _.merge(args.state.complete, args.elements);
            if(args.options.refresh !== false){
                pendingActivities.deletePendingActivities(_.merge({}, args, {
                    options: {
                        refresh: true
                    }
                }));
            }
        },
        deleteCompleteActivities: function(args){
            this.handleState(args);
            checkTypes.assert.object(args);
            checkTypes.assert.object(args.elements);
            if(!checkTypes.object(args.options)){
                args.options = {}
            }
            _.forEach(args.elements, function(v, k){
                delete this.state.complete[v];
            })

            if(args.options.refresh !== false){
                pendingActivities.addPendingActivities(_.merge({}, args, {
                    options: {
                        refresh: true
                    }
                }));
            }
            
        }
    }
}